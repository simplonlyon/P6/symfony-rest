<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Student;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/rest/student", name="rest_student")
 */
class RestStudentController extends Controller
{
    private $serializer;
    //On injecte le serializer dans le constructeur de la classe
    //contrôleur car on devra s'en servir dans toutes les méthodes
    //de celle ci.
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function index()
    {
        // //On crée une réponse http
        // $response = new Response();
        // //On indique que son contenu sera du json
        // $response->headers->set("Content-Type", "application/json");
        // //On indique que le code de retour est 200 ok
        // $response->setStatusCode(200);
        // //On met comme contenu un petit tableau encodé en json
        // $response->setContent(json_encode(["ga" => "bu"]));
        
        $repo = $this->getDoctrine()->getRepository(Student::class);
        
        //On utilise le serializer de symfony pour transfomer une
        //entité (ou array d'entités) php au format json
        $json = $this->serializer->serialize($repo->findAll(), "json");
        //On utilise la méthode static fromJsonString de la classe
        //JsonResponse pour créer une réponse HTTP en json à partir
        //de données déjà au format JSON
        return JsonResponse::fromJsonString($json);
    }
    
    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request) {
        //On récupère le body de la request (les données de celle ci)
        $body = $request->getContent();
        //On transforme ce body en instance de Student depuis le format json
        $student = $this->serializer->deserialize($body, Student::class, "json");
        //On fait persister le student via doctrine
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($student);
        $manager->flush();
        //On envoie une réponse pour dire que c'est ok et dire l'id qui vient d'être ajouté
        return new JsonResponse(["id" => $student->getId()]);
    }

    /**
     * @Route("/{student}", methods={"GET"})
     */
    public function single(Student $student) {
        $json = $this->serializer->serialize($student,"json");
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{student}", methods={"DELETE"})
     */
    public function remove(Student $student) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($student);
        $manager->flush();
        return new Response("", 204);
    }

    /**
     * @Route("/{student}", methods={"PUT"})
     */
    public function update(Student $student, Request $request) {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Student::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $student->setName($updated->getName());
        $student->setSurname($updated->getSurname());
        $student->setLevel($updated->getLevel());
        $student->setTech($updated->getTech());
        
        $manager->flush();
        return new Response("", 204);
    }

    
}
